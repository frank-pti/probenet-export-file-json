using Internationalization;
using System.Collections.Generic;
using System.ComponentModel;
/*
 * ProbeNet Json File Export
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.IO;

namespace ProbeNet.Export.File.Json
{
    [TranslatableCaption(Constants.ExportDomain, "ProbeNet Data")]
    [FileFilterAttribute("probenet", "text/vnd.probenet")]
    [Description("ProbeNet")]
    public class JsonFileExport : FileExport
    {
        public JsonFileExport(SettingsProvider settingsProvider) :
            base(settingsProvider)
        {
        }

        #region implemented abstract members of ProbeNet.Export.File.FileExport
        public override void Export(
            I18n i18n, MultipleSourcesDataModelBase model, IList<IList<string>> measurementIdsForExport, string filename)
        {
            Stream saveStream = GetExportStream(filename);
            model.MeasurementIdsForExport = measurementIdsForExport;
            model.SerializeStreamJson(saveStream);
            saveStream.Close();
        }
        #endregion

        protected virtual Stream GetExportStream(string filename)
        {
            return new FileStream(filename, FileMode.Create);
        }
    }
}

