/*
 * ProbeNet Json File Export
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Internationalization;
using System.IO;
using ProbeNet.Export.File;
using System.IO.Compression;
using System.ComponentModel;

namespace ProbeNet.Export.File.Json
{
    [TranslatableCaption(Constants.ExportDomain, "ProbeNet Data, compressed")]
    [FileFilterAttribute("probenet.gz", "text/vnd.probenet.gzipped")]
    [Description("ProbeNetCompressed")]
    public class CompressedJsonFileExport: JsonFileExport
    {
        public CompressedJsonFileExport (SettingsProvider settingsProvider):
            base(settingsProvider)
        {
        }

        protected override Stream GetExportStream (string filename)
        {
            return new GZipStream(base.GetExportStream (filename), CompressionMode.Compress);
        }
    }
}

